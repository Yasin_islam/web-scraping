# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class QuotesProjectItem(scrapy.Item):
    # define the fields for your item here like:
    quotes_item = scrapy.Field()
    authors_item = scrapy.Field()
    tags_item = scrapy.Field()
