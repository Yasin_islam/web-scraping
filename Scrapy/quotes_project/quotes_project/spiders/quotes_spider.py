import scrapy
from ..items import QuotesProjectItem

class QuotesSpiderSpider(scrapy.Spider):
    name = 'quotes_spider'
    start_urls = ['https://quotes.toscrape.com//']

    def parse(self, response):
        items = QuotesProjectItem()
        #all_div_quotes = response.xpath("//div[@class='quote']").extract()

        quotes_text =  response.xpath("//span[@class='text']/text()").extract()
        authors_text =  response.xpath("//small[@class='author']/text()").extract()
        tags_text = response.xpath("//a[@class='tag']/text()").extract()

        #data will go to items file
        items['quotes_item'] = quotes_text
        items['authors_item'] = authors_text
        items['tags_item'] = tags_text

        yield items

        next_page = response.xpath("//li[@class='next']/a/@href").extract()

        if next_page is not None:
            yield response.follow(next_page, callback=self.parse())