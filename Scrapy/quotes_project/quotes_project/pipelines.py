# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import sqlite3
class QuotesProjectPipeline:
    def __init__(self):
        self.create_connection()
        self.create_table()
    def create_connection(self):
        self.conn = sqlite3.connect("quotes.db")
        self.curr = self.conn.cursor()
    def create_table(self):
        self.curr.execute(""" DROP TABLE IF EXISTS quotes_tb """)
        self.curr.execute("""create table quotes_tb(
        quotes text,
        author text,
        tags text
        )""")

    def process_item(self, item, spider):
        self.storing_quotes_data(item)
        return item
    def storing_quotes_data(self,item):
        self.curr.execute("""insert into quotes_tb values (?,?,?)""",(
            item['quotes_item'][0],
            item['authors_item'][0],
            item['tags_item'][0]
        ))
        self.conn.commit()