# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import sqlite3

class ImdbMoviesPipeline:
    def __init__(self):
        self.create_connection()
        self.create_table()
    def create_connection(self):
        self.conn = sqlite3.connect("imdb.db")
        self.curr = self.conn.cursor()
    def create_table(self):
        self.curr.execute(""" DROP TABLE IF EXISTS imdb_tb """)
        self.curr.execute("""create table imdb_tb(
        movie_name text,
        year text,
        rating text,
        image_link  text
        )""")


    def process_item(self, item, spider):
        self.storing_imdb_data(item)
        return item
    def storing_imdb_data(self,item):
        self.curr.execute("""insert into imdb_tb values (?,?,?,?)""",(
            item['name_item'][0],
            item['year_item'][0],
            item['rating_item'][0],
            item['imagelink_item'][0]
        ))
        self.conn.commit()