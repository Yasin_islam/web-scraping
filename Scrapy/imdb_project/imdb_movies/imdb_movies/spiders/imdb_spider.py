import scrapy
from ..items import ImdbMoviesItem

class ImdbSpiderSpider(scrapy.Spider):
    name = 'imdb_spider'
    page_number = 51
    start_urls = ['https://www.imdb.com/search/title/?groups=top_250&sort=user_rating,desc&start=01&ref_=adv_nxt']

    def parse(self, response):
        items = ImdbMoviesItem()
        div = response.css(".mode-advanced")
        for x in div:
            name_text =  x.css(".lister-item-header a::text").extract()
            year_text =  x.css(".text-muted.unbold::text").extract()
            rating_text = x.css("strong::text").extract()
            imagelink_text = x.css(".loadlate::attr(src)").extract()

            items['name_item'] = name_text
            items['year_item'] = year_text
            items['rating_item'] = rating_text
            items['imagelink_item'] = imagelink_text

            yield items

        next_page = "https://www.imdb.com/search/title/?groups=top_250&sort=user_rating,desc&start="+ str(ImdbSpiderSpider.page_number) +"&ref_=adv_nxt"
        if ImdbSpiderSpider.page_number <=201:
            ImdbSpiderSpider.page_number +=50
            yield response.follow(next_page, callback=self.parse)





